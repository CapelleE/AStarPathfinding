# astar.py

from mymap import MyMap

try:
    from Queue import Queue
except ImportError:
    from queue import PriorityQueue

m = MyMap()
m.generateMap()

# print(m)

start = m.getTileAt(0, 0)
goal = m.getTileAt(6, 6)

frontier = PriorityQueue()
frontier.put(start)

came_from = {}
came_from[start] = None

cost_so_far = {}
cost_so_far[start] = 0

while not frontier.empty():
    current = frontier.get()

    if current == goal:
        break

    for n in m.getNeighbours(current, True):
        new_cost = cost_so_far.get(current, 0) + current.tileCost
        if n not in cost_so_far or new_cost < cost_so_far[n]:
            if n not in came_from:
                frontier.put(n)
                came_from[n] = current

current = goal
path = [current]
while current != start:
    current = came_from.get(current)
    path.append(current)
    current.graphic = "x"
path.reverse()

goal.graphic = "G"
start.graphic = "S"

print(path)
print(m)
