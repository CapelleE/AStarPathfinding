# map.py

from collections import OrderedDict

IMPASSABLE_TERRAIN = 9999999


class Tile():
    def __init__(self, x: int, y: int, graphic: str):
        self.x = x
        self.y = y
        self.graphic = graphic
        self.tileCost = self.getTileCost()

    def getTileCost(self):
        if self.graphic == "_":
            return 1
        elif self.graphic == "#":
            return IMPASSABLE_TERRAIN
        else:
            return IMPASSABLE_TERRAIN

    def __lt__(self, other):
        return other.tileCost > self.tileCost

    def __hash__(self):
        return (self.x * 22) + (self.y * 33)

    def __str__(self):
        return self.graphic

    def __repr__(self):
        return self.__str__()


class MyMap():
    def __init__(self):
        self.map = [[]]
        self.height = 0
        self.width = 0

    def generateMap(self, sourceMap=None):
        if sourceMap == None:
            sourceMap = [
                ["_", "_", "#", "_", "_", "_", "_", "_", "_", "_"],
                ["_", "_", "#", "_", "#", "#", "#", "#", "#", "_"],
                ["_", "_", "#", "_", "#", "_", "_", "_", "#", "_"],
                ["_", "_", "#", "_", "_", "_", "_", "_", "#", "_"],
                ["_", "_", "#", "#", "#", "#", "#", "#", "#", "_"],
                ["_", "_", "_", "_", "_", "_", "_", "_", "_", "_"],
                ["_", "_", "_", "_", "_", "_", "_", "_", "_", "_"],
                ["_", "_", "#", "#", "#", "_", "_", "_", "_", "_"],
                ["_", "_", "#", "_", "#", "#", "#", "#", "_", "#"],
                ["_", "_", "#", "_", "_", "_", "_", "_", "_", "#"]
            ]

        self.width = len(sourceMap[0])
        self.height = len(sourceMap)

        result = []
        for y in range(0, self.height):
            row = []
            for x in range(0, self.width):
                row.append(None)
            result.append(row)

        cx = 0
        cy = 0
        for y in range(0, self.height):
            for x in range(0, self.width):
                result[y][x] = Tile(cx, cy, sourceMap[y][x])
                cx = cx + 1
            cy = cy + 1
            cx = 0

        self.map = result

    def getTileAt(self, x: int, y: int):
        return self.map[y][x] or None

    def getNeighbours(self, tile: Tile, convertToList = False):
        x = tile.x
        y = tile.y
        result = OrderedDict(
            [
                ("center", (x, y)),
            ]
        )

        left = x - 1
        right = x + 1
        top = y - 1
        bottom = y + 1

        if left >= 0 and left < self.width:
            result["left"] = self.map[y][left]
        if right >= 0 and right < self.width:
            result["right"] = self.map[y][right]
        if top >= 0 and top < self.height:
            result["top"] = self.map[top][x]
        if bottom >= 0 and bottom < self.height:
            result["bottom"] = self.map[bottom][x]

        if convertToList:
            return self.dictToList(result)
        else:
            return result

    def dictToList(self, tilesDict):
        result = []

        if tilesDict.get("top", None) != None:
            result.append(tilesDict["top"])
        if tilesDict.get("right", None) != None:
            result.append(tilesDict["right"])
        if tilesDict.get("bottom", None) != None:
            result.append(tilesDict["bottom"])
        if tilesDict.get("left", None) != None:
            result.append(tilesDict["left"])

        return result

    def __str__(self):
        ans = ""
        for row in self.map:
            for t in row:
                ans = ans + "{}".format(t) if t != None else ' '
            ans = ans + "\n"
        return ans

    def __repr__(self):
        return self.__str__()
